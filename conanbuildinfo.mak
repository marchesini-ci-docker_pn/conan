#-------------------------------------------------------------------#
#             Makefile variables from Conan Dependencies            #
#-------------------------------------------------------------------#

CONAN_ROOT_POCO ?=  \
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c

CONAN_SYSROOT_POCO ?=  \


CONAN_INCLUDE_DIRS_POCO +=  \
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c/include

CONAN_LIB_DIRS_POCO +=  \
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c/lib

CONAN_BIN_DIRS_POCO += 

CONAN_BUILD_DIRS_POCO += 

CONAN_RES_DIRS_POCO += 

CONAN_LIBS_POCO +=  \
PocoDataMySQL \
PocoDataPostgreSQL \
PocoDataSQLite \
PocoEncodings \
PocoJWT \
PocoMongoDB \
PocoNetSSL \
PocoCrypto \
PocoRedis \
PocoNet \
PocoZip \
PocoUtil \
PocoJSON \
PocoXML \
PocoActiveRecord \
PocoData \
PocoFoundation

CONAN_SYSTEM_LIBS_POCO +=  \
pthread \
dl \
rt

CONAN_DEFINES_POCO +=  \
POCO_STATIC=ON \
POCO_UNBUNDLED

CONAN_CFLAGS_POCO += 

CONAN_CXXFLAGS_POCO += 

CONAN_SHAREDLINKFLAGS_POCO += 

CONAN_EXELINKFLAGS_POCO += 

CONAN_FRAMEWORKS_POCO += 

CONAN_FRAMEWORK_PATHS_POCO += 

CONAN_ROOT_PCRE2 ?=  \
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11

CONAN_SYSROOT_PCRE2 ?=  \


CONAN_INCLUDE_DIRS_PCRE2 +=  \
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/include

CONAN_LIB_DIRS_PCRE2 +=  \
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/lib

CONAN_BIN_DIRS_PCRE2 +=  \
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/bin

CONAN_BUILD_DIRS_PCRE2 += 

CONAN_RES_DIRS_PCRE2 += 

CONAN_LIBS_PCRE2 +=  \
pcre2-posix \
pcre2-8 \
pcre2-16 \
pcre2-32

CONAN_SYSTEM_LIBS_PCRE2 += 

CONAN_DEFINES_PCRE2 +=  \
PCRE2_STATIC

CONAN_CFLAGS_PCRE2 += 

CONAN_CXXFLAGS_PCRE2 += 

CONAN_SHAREDLINKFLAGS_PCRE2 += 

CONAN_EXELINKFLAGS_PCRE2 += 

CONAN_FRAMEWORKS_PCRE2 += 

CONAN_FRAMEWORK_PATHS_PCRE2 += 

CONAN_ROOT_EXPAT ?=  \
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f

CONAN_SYSROOT_EXPAT ?=  \


CONAN_INCLUDE_DIRS_EXPAT +=  \
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/include

CONAN_LIB_DIRS_EXPAT +=  \
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/lib

CONAN_BIN_DIRS_EXPAT += 

CONAN_BUILD_DIRS_EXPAT +=  \
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/

CONAN_RES_DIRS_EXPAT += 

CONAN_LIBS_EXPAT +=  \
expat

CONAN_SYSTEM_LIBS_EXPAT += 

CONAN_DEFINES_EXPAT +=  \
XML_STATIC

CONAN_CFLAGS_EXPAT += 

CONAN_CXXFLAGS_EXPAT += 

CONAN_SHAREDLINKFLAGS_EXPAT += 

CONAN_EXELINKFLAGS_EXPAT += 

CONAN_FRAMEWORKS_EXPAT += 

CONAN_FRAMEWORK_PATHS_EXPAT += 

CONAN_ROOT_SQLITE3 ?=  \
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8

CONAN_SYSROOT_SQLITE3 ?=  \


CONAN_INCLUDE_DIRS_SQLITE3 +=  \
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/include

CONAN_LIB_DIRS_SQLITE3 +=  \
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/lib

CONAN_BIN_DIRS_SQLITE3 +=  \
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/bin

CONAN_BUILD_DIRS_SQLITE3 += 

CONAN_RES_DIRS_SQLITE3 += 

CONAN_LIBS_SQLITE3 +=  \
sqlite3

CONAN_SYSTEM_LIBS_SQLITE3 +=  \
pthread \
dl \
m

CONAN_DEFINES_SQLITE3 += 

CONAN_CFLAGS_SQLITE3 += 

CONAN_CXXFLAGS_SQLITE3 += 

CONAN_SHAREDLINKFLAGS_SQLITE3 += 

CONAN_EXELINKFLAGS_SQLITE3 += 

CONAN_FRAMEWORKS_SQLITE3 += 

CONAN_FRAMEWORK_PATHS_SQLITE3 += 

CONAN_ROOT_LIBPQ ?=  \
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

CONAN_SYSROOT_LIBPQ ?=  \


CONAN_INCLUDE_DIRS_LIBPQ +=  \
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

CONAN_LIB_DIRS_LIBPQ +=  \
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

CONAN_BIN_DIRS_LIBPQ +=  \
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/bin

CONAN_BUILD_DIRS_LIBPQ +=  \
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

CONAN_RES_DIRS_LIBPQ += 

CONAN_LIBS_LIBPQ +=  \
pq \
pgcommon \
pgcommon_shlib \
pgport \
pgport_shlib

CONAN_SYSTEM_LIBS_LIBPQ +=  \
pthread

CONAN_DEFINES_LIBPQ += 

CONAN_CFLAGS_LIBPQ += 

CONAN_CXXFLAGS_LIBPQ += 

CONAN_SHAREDLINKFLAGS_LIBPQ += 

CONAN_EXELINKFLAGS_LIBPQ += 

CONAN_FRAMEWORKS_LIBPQ += 

CONAN_FRAMEWORK_PATHS_LIBPQ += 

CONAN_ROOT_LIBMYSQLCLIENT ?=  \
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126

CONAN_SYSROOT_LIBMYSQLCLIENT ?=  \


CONAN_INCLUDE_DIRS_LIBMYSQLCLIENT +=  \
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/include

CONAN_LIB_DIRS_LIBMYSQLCLIENT +=  \
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/lib

CONAN_BIN_DIRS_LIBMYSQLCLIENT +=  \
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/bin

CONAN_BUILD_DIRS_LIBMYSQLCLIENT +=  \
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/

CONAN_RES_DIRS_LIBMYSQLCLIENT += 

CONAN_LIBS_LIBMYSQLCLIENT +=  \
mysqlclient

CONAN_SYSTEM_LIBS_LIBMYSQLCLIENT +=  \
stdc++ \
m \
resolv

CONAN_DEFINES_LIBMYSQLCLIENT += 

CONAN_CFLAGS_LIBMYSQLCLIENT += 

CONAN_CXXFLAGS_LIBMYSQLCLIENT += 

CONAN_SHAREDLINKFLAGS_LIBMYSQLCLIENT += 

CONAN_EXELINKFLAGS_LIBMYSQLCLIENT += 

CONAN_FRAMEWORKS_LIBMYSQLCLIENT += 

CONAN_FRAMEWORK_PATHS_LIBMYSQLCLIENT += 

CONAN_ROOT_ZLIB ?=  \
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

CONAN_SYSROOT_ZLIB ?=  \


CONAN_INCLUDE_DIRS_ZLIB +=  \
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

CONAN_LIB_DIRS_ZLIB +=  \
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

CONAN_BIN_DIRS_ZLIB += 

CONAN_BUILD_DIRS_ZLIB +=  \
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

CONAN_RES_DIRS_ZLIB += 

CONAN_LIBS_ZLIB +=  \
z

CONAN_SYSTEM_LIBS_ZLIB += 

CONAN_DEFINES_ZLIB += 

CONAN_CFLAGS_ZLIB += 

CONAN_CXXFLAGS_ZLIB += 

CONAN_SHAREDLINKFLAGS_ZLIB += 

CONAN_EXELINKFLAGS_ZLIB += 

CONAN_FRAMEWORKS_ZLIB += 

CONAN_FRAMEWORK_PATHS_ZLIB += 

CONAN_ROOT_BZIP2 ?=  \
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e

CONAN_SYSROOT_BZIP2 ?=  \


CONAN_INCLUDE_DIRS_BZIP2 +=  \
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/include

CONAN_LIB_DIRS_BZIP2 +=  \
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/lib

CONAN_BIN_DIRS_BZIP2 +=  \
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/bin

CONAN_BUILD_DIRS_BZIP2 +=  \
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/

CONAN_RES_DIRS_BZIP2 += 

CONAN_LIBS_BZIP2 +=  \
bz2

CONAN_SYSTEM_LIBS_BZIP2 += 

CONAN_DEFINES_BZIP2 += 

CONAN_CFLAGS_BZIP2 += 

CONAN_CXXFLAGS_BZIP2 += 

CONAN_SHAREDLINKFLAGS_BZIP2 += 

CONAN_EXELINKFLAGS_BZIP2 += 

CONAN_FRAMEWORKS_BZIP2 += 

CONAN_FRAMEWORK_PATHS_BZIP2 += 

CONAN_ROOT_OPENSSL ?=  \
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

CONAN_SYSROOT_OPENSSL ?=  \


CONAN_INCLUDE_DIRS_OPENSSL +=  \
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

CONAN_LIB_DIRS_OPENSSL +=  \
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

CONAN_BIN_DIRS_OPENSSL +=  \
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/bin

CONAN_BUILD_DIRS_OPENSSL += 

CONAN_RES_DIRS_OPENSSL += 

CONAN_LIBS_OPENSSL +=  \
ssl \
crypto

CONAN_SYSTEM_LIBS_OPENSSL +=  \
dl \
pthread \
rt

CONAN_DEFINES_OPENSSL += 

CONAN_CFLAGS_OPENSSL += 

CONAN_CXXFLAGS_OPENSSL += 

CONAN_SHAREDLINKFLAGS_OPENSSL += 

CONAN_EXELINKFLAGS_OPENSSL += 

CONAN_FRAMEWORKS_OPENSSL += 

CONAN_FRAMEWORK_PATHS_OPENSSL += 

CONAN_ROOT_ZSTD ?=  \
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6

CONAN_SYSROOT_ZSTD ?=  \


CONAN_INCLUDE_DIRS_ZSTD +=  \
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6/include

CONAN_LIB_DIRS_ZSTD +=  \
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6/lib

CONAN_BIN_DIRS_ZSTD += 

CONAN_BUILD_DIRS_ZSTD += 

CONAN_RES_DIRS_ZSTD += 

CONAN_LIBS_ZSTD +=  \
zstd

CONAN_SYSTEM_LIBS_ZSTD +=  \
pthread

CONAN_DEFINES_ZSTD += 

CONAN_CFLAGS_ZSTD += 

CONAN_CXXFLAGS_ZSTD += 

CONAN_SHAREDLINKFLAGS_ZSTD += 

CONAN_EXELINKFLAGS_ZSTD += 

CONAN_FRAMEWORKS_ZSTD += 

CONAN_FRAMEWORK_PATHS_ZSTD += 

CONAN_ROOT_LZ4 ?=  \
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

CONAN_SYSROOT_LZ4 ?=  \


CONAN_INCLUDE_DIRS_LZ4 +=  \
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

CONAN_LIB_DIRS_LZ4 +=  \
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

CONAN_BIN_DIRS_LZ4 += 

CONAN_BUILD_DIRS_LZ4 +=  \
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

CONAN_RES_DIRS_LZ4 += 

CONAN_LIBS_LZ4 +=  \
lz4

CONAN_SYSTEM_LIBS_LZ4 += 

CONAN_DEFINES_LZ4 += 

CONAN_CFLAGS_LZ4 += 

CONAN_CXXFLAGS_LZ4 += 

CONAN_SHAREDLINKFLAGS_LZ4 += 

CONAN_EXELINKFLAGS_LZ4 += 

CONAN_FRAMEWORKS_LZ4 += 

CONAN_FRAMEWORK_PATHS_LZ4 += 

CONAN_ROOT +=  \
$(CONAN_ROOT_POCO) \
$(CONAN_ROOT_PCRE2) \
$(CONAN_ROOT_EXPAT) \
$(CONAN_ROOT_SQLITE3) \
$(CONAN_ROOT_LIBPQ) \
$(CONAN_ROOT_LIBMYSQLCLIENT) \
$(CONAN_ROOT_ZLIB) \
$(CONAN_ROOT_BZIP2) \
$(CONAN_ROOT_OPENSSL) \
$(CONAN_ROOT_ZSTD) \
$(CONAN_ROOT_LZ4)

CONAN_SYSROOT +=  \
$(CONAN_SYSROOT_POCO) \
$(CONAN_SYSROOT_PCRE2) \
$(CONAN_SYSROOT_EXPAT) \
$(CONAN_SYSROOT_SQLITE3) \
$(CONAN_SYSROOT_LIBPQ) \
$(CONAN_SYSROOT_LIBMYSQLCLIENT) \
$(CONAN_SYSROOT_ZLIB) \
$(CONAN_SYSROOT_BZIP2) \
$(CONAN_SYSROOT_OPENSSL) \
$(CONAN_SYSROOT_ZSTD) \
$(CONAN_SYSROOT_LZ4)

CONAN_INCLUDE_DIRS +=  \
$(CONAN_INCLUDE_DIRS_POCO) \
$(CONAN_INCLUDE_DIRS_PCRE2) \
$(CONAN_INCLUDE_DIRS_EXPAT) \
$(CONAN_INCLUDE_DIRS_SQLITE3) \
$(CONAN_INCLUDE_DIRS_LIBPQ) \
$(CONAN_INCLUDE_DIRS_LIBMYSQLCLIENT) \
$(CONAN_INCLUDE_DIRS_ZLIB) \
$(CONAN_INCLUDE_DIRS_BZIP2) \
$(CONAN_INCLUDE_DIRS_OPENSSL) \
$(CONAN_INCLUDE_DIRS_ZSTD) \
$(CONAN_INCLUDE_DIRS_LZ4)

CONAN_LIB_DIRS +=  \
$(CONAN_LIB_DIRS_POCO) \
$(CONAN_LIB_DIRS_PCRE2) \
$(CONAN_LIB_DIRS_EXPAT) \
$(CONAN_LIB_DIRS_SQLITE3) \
$(CONAN_LIB_DIRS_LIBPQ) \
$(CONAN_LIB_DIRS_LIBMYSQLCLIENT) \
$(CONAN_LIB_DIRS_ZLIB) \
$(CONAN_LIB_DIRS_BZIP2) \
$(CONAN_LIB_DIRS_OPENSSL) \
$(CONAN_LIB_DIRS_ZSTD) \
$(CONAN_LIB_DIRS_LZ4)

CONAN_BIN_DIRS +=  \
$(CONAN_BIN_DIRS_POCO) \
$(CONAN_BIN_DIRS_PCRE2) \
$(CONAN_BIN_DIRS_EXPAT) \
$(CONAN_BIN_DIRS_SQLITE3) \
$(CONAN_BIN_DIRS_LIBPQ) \
$(CONAN_BIN_DIRS_LIBMYSQLCLIENT) \
$(CONAN_BIN_DIRS_ZLIB) \
$(CONAN_BIN_DIRS_BZIP2) \
$(CONAN_BIN_DIRS_OPENSSL) \
$(CONAN_BIN_DIRS_ZSTD) \
$(CONAN_BIN_DIRS_LZ4)

CONAN_BUILD_DIRS +=  \
$(CONAN_BUILD_DIRS_POCO) \
$(CONAN_BUILD_DIRS_PCRE2) \
$(CONAN_BUILD_DIRS_EXPAT) \
$(CONAN_BUILD_DIRS_SQLITE3) \
$(CONAN_BUILD_DIRS_LIBPQ) \
$(CONAN_BUILD_DIRS_LIBMYSQLCLIENT) \
$(CONAN_BUILD_DIRS_ZLIB) \
$(CONAN_BUILD_DIRS_BZIP2) \
$(CONAN_BUILD_DIRS_OPENSSL) \
$(CONAN_BUILD_DIRS_ZSTD) \
$(CONAN_BUILD_DIRS_LZ4)

CONAN_RES_DIRS +=  \
$(CONAN_RES_DIRS_POCO) \
$(CONAN_RES_DIRS_PCRE2) \
$(CONAN_RES_DIRS_EXPAT) \
$(CONAN_RES_DIRS_SQLITE3) \
$(CONAN_RES_DIRS_LIBPQ) \
$(CONAN_RES_DIRS_LIBMYSQLCLIENT) \
$(CONAN_RES_DIRS_ZLIB) \
$(CONAN_RES_DIRS_BZIP2) \
$(CONAN_RES_DIRS_OPENSSL) \
$(CONAN_RES_DIRS_ZSTD) \
$(CONAN_RES_DIRS_LZ4)

CONAN_LIBS +=  \
$(CONAN_LIBS_POCO) \
$(CONAN_LIBS_PCRE2) \
$(CONAN_LIBS_EXPAT) \
$(CONAN_LIBS_SQLITE3) \
$(CONAN_LIBS_LIBPQ) \
$(CONAN_LIBS_LIBMYSQLCLIENT) \
$(CONAN_LIBS_ZLIB) \
$(CONAN_LIBS_BZIP2) \
$(CONAN_LIBS_OPENSSL) \
$(CONAN_LIBS_ZSTD) \
$(CONAN_LIBS_LZ4)

CONAN_DEFINES +=  \
$(CONAN_DEFINES_POCO) \
$(CONAN_DEFINES_PCRE2) \
$(CONAN_DEFINES_EXPAT) \
$(CONAN_DEFINES_SQLITE3) \
$(CONAN_DEFINES_LIBPQ) \
$(CONAN_DEFINES_LIBMYSQLCLIENT) \
$(CONAN_DEFINES_ZLIB) \
$(CONAN_DEFINES_BZIP2) \
$(CONAN_DEFINES_OPENSSL) \
$(CONAN_DEFINES_ZSTD) \
$(CONAN_DEFINES_LZ4)

CONAN_CFLAGS +=  \
$(CONAN_CFLAGS_POCO) \
$(CONAN_CFLAGS_PCRE2) \
$(CONAN_CFLAGS_EXPAT) \
$(CONAN_CFLAGS_SQLITE3) \
$(CONAN_CFLAGS_LIBPQ) \
$(CONAN_CFLAGS_LIBMYSQLCLIENT) \
$(CONAN_CFLAGS_ZLIB) \
$(CONAN_CFLAGS_BZIP2) \
$(CONAN_CFLAGS_OPENSSL) \
$(CONAN_CFLAGS_ZSTD) \
$(CONAN_CFLAGS_LZ4)

CONAN_CXXFLAGS +=  \
$(CONAN_CXXFLAGS_POCO) \
$(CONAN_CXXFLAGS_PCRE2) \
$(CONAN_CXXFLAGS_EXPAT) \
$(CONAN_CXXFLAGS_SQLITE3) \
$(CONAN_CXXFLAGS_LIBPQ) \
$(CONAN_CXXFLAGS_LIBMYSQLCLIENT) \
$(CONAN_CXXFLAGS_ZLIB) \
$(CONAN_CXXFLAGS_BZIP2) \
$(CONAN_CXXFLAGS_OPENSSL) \
$(CONAN_CXXFLAGS_ZSTD) \
$(CONAN_CXXFLAGS_LZ4)

CONAN_SHAREDLINKFLAGS +=  \
$(CONAN_SHAREDLINKFLAGS_POCO) \
$(CONAN_SHAREDLINKFLAGS_PCRE2) \
$(CONAN_SHAREDLINKFLAGS_EXPAT) \
$(CONAN_SHAREDLINKFLAGS_SQLITE3) \
$(CONAN_SHAREDLINKFLAGS_LIBPQ) \
$(CONAN_SHAREDLINKFLAGS_LIBMYSQLCLIENT) \
$(CONAN_SHAREDLINKFLAGS_ZLIB) \
$(CONAN_SHAREDLINKFLAGS_BZIP2) \
$(CONAN_SHAREDLINKFLAGS_OPENSSL) \
$(CONAN_SHAREDLINKFLAGS_ZSTD) \
$(CONAN_SHAREDLINKFLAGS_LZ4)

CONAN_EXELINKFLAGS +=  \
$(CONAN_EXELINKFLAGS_POCO) \
$(CONAN_EXELINKFLAGS_PCRE2) \
$(CONAN_EXELINKFLAGS_EXPAT) \
$(CONAN_EXELINKFLAGS_SQLITE3) \
$(CONAN_EXELINKFLAGS_LIBPQ) \
$(CONAN_EXELINKFLAGS_LIBMYSQLCLIENT) \
$(CONAN_EXELINKFLAGS_ZLIB) \
$(CONAN_EXELINKFLAGS_BZIP2) \
$(CONAN_EXELINKFLAGS_OPENSSL) \
$(CONAN_EXELINKFLAGS_ZSTD) \
$(CONAN_EXELINKFLAGS_LZ4)

CONAN_FRAMEWORKS +=  \
$(CONAN_FRAMEWORKS_POCO) \
$(CONAN_FRAMEWORKS_PCRE2) \
$(CONAN_FRAMEWORKS_EXPAT) \
$(CONAN_FRAMEWORKS_SQLITE3) \
$(CONAN_FRAMEWORKS_LIBPQ) \
$(CONAN_FRAMEWORKS_LIBMYSQLCLIENT) \
$(CONAN_FRAMEWORKS_ZLIB) \
$(CONAN_FRAMEWORKS_BZIP2) \
$(CONAN_FRAMEWORKS_OPENSSL) \
$(CONAN_FRAMEWORKS_ZSTD) \
$(CONAN_FRAMEWORKS_LZ4)

CONAN_FRAMEWORK_PATHS +=  \
$(CONAN_FRAMEWORK_PATHS_POCO) \
$(CONAN_FRAMEWORK_PATHS_PCRE2) \
$(CONAN_FRAMEWORK_PATHS_EXPAT) \
$(CONAN_FRAMEWORK_PATHS_SQLITE3) \
$(CONAN_FRAMEWORK_PATHS_LIBPQ) \
$(CONAN_FRAMEWORK_PATHS_LIBMYSQLCLIENT) \
$(CONAN_FRAMEWORK_PATHS_ZLIB) \
$(CONAN_FRAMEWORK_PATHS_BZIP2) \
$(CONAN_FRAMEWORK_PATHS_OPENSSL) \
$(CONAN_FRAMEWORK_PATHS_ZSTD) \
$(CONAN_FRAMEWORK_PATHS_LZ4)

CONAN_SYSTEM_LIBS +=  \
$(CONAN_SYSTEM_LIBS_POCO) \
$(CONAN_SYSTEM_LIBS_PCRE2) \
$(CONAN_SYSTEM_LIBS_EXPAT) \
$(CONAN_SYSTEM_LIBS_SQLITE3) \
$(CONAN_SYSTEM_LIBS_LIBPQ) \
$(CONAN_SYSTEM_LIBS_LIBMYSQLCLIENT) \
$(CONAN_SYSTEM_LIBS_ZLIB) \
$(CONAN_SYSTEM_LIBS_BZIP2) \
$(CONAN_SYSTEM_LIBS_OPENSSL) \
$(CONAN_SYSTEM_LIBS_ZSTD) \
$(CONAN_SYSTEM_LIBS_LZ4)

#-------------------------------------------------------------------#

