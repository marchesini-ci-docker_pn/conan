[includedirs]
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c/include
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/include
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/include
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/include
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/include
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/include
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6/include
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

[libdirs]
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c/lib
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/lib
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/lib
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/lib
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/lib
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/lib
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6/lib
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

[bindirs]
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/bin
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/bin
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/bin
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/bin
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/bin
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/bin

[resdirs]


[builddirs]
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

[libs]
PocoDataMySQL
PocoDataPostgreSQL
PocoDataSQLite
PocoEncodings
PocoJWT
PocoMongoDB
PocoNetSSL
PocoCrypto
PocoRedis
PocoNet
PocoZip
PocoUtil
PocoJSON
PocoXML
PocoActiveRecord
PocoData
PocoFoundation
pcre2-posix
pcre2-8
pcre2-16
pcre2-32
expat
sqlite3
pq
pgcommon
pgcommon_shlib
pgport
pgport_shlib
mysqlclient
z
bz2
ssl
crypto
zstd
lz4

[system_libs]
stdc++
m
resolv
dl
rt
pthread

[defines]
XML_STATIC
PCRE2_STATIC
POCO_STATIC=ON
POCO_UNBUNDLED

[cppflags]


[cxxflags]


[cflags]


[sharedlinkflags]


[exelinkflags]


[sysroot]


[frameworks]


[frameworkdirs]



[includedirs_poco]
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c/include

[libdirs_poco]
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c/lib

[bindirs_poco]


[resdirs_poco]


[builddirs_poco]


[libs_poco]
PocoDataMySQL
PocoDataPostgreSQL
PocoDataSQLite
PocoEncodings
PocoJWT
PocoMongoDB
PocoNetSSL
PocoCrypto
PocoRedis
PocoNet
PocoZip
PocoUtil
PocoJSON
PocoXML
PocoActiveRecord
PocoData
PocoFoundation

[system_libs_poco]
pthread
dl
rt

[defines_poco]
POCO_STATIC=ON
POCO_UNBUNDLED

[cppflags_poco]


[cxxflags_poco]


[cflags_poco]


[sharedlinkflags_poco]


[exelinkflags_poco]


[sysroot_poco]


[frameworks_poco]


[frameworkdirs_poco]


[rootpath_poco]
/root/.conan/data/poco/1.12.2/_/_/package/1e6fb57c93b6b3c8a3147d7c537eb19271c4f94c

[name_poco]
poco

[version_poco]
1.12.2

[generatornames_poco]
cmake_find_package=Poco
cmake_find_package_multi=Poco

[generatorfilenames_poco]
cmake_find_package=Poco
cmake_find_package_multi=Poco


[includedirs_pcre2]
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/include

[libdirs_pcre2]
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/lib

[bindirs_pcre2]
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/bin

[resdirs_pcre2]


[builddirs_pcre2]


[libs_pcre2]
pcre2-posix
pcre2-8
pcre2-16
pcre2-32

[system_libs_pcre2]


[defines_pcre2]
PCRE2_STATIC

[cppflags_pcre2]


[cxxflags_pcre2]


[cflags_pcre2]


[sharedlinkflags_pcre2]


[exelinkflags_pcre2]


[sysroot_pcre2]


[frameworks_pcre2]


[frameworkdirs_pcre2]


[rootpath_pcre2]
/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11

[name_pcre2]
pcre2

[version_pcre2]
10.40

[generatornames_pcre2]
cmake_find_package=PCRE2
cmake_find_package_multi=PCRE2
pkg_config=libpcre2

[generatorfilenames_pcre2]



[includedirs_expat]
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/include

[libdirs_expat]
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/lib

[bindirs_expat]


[resdirs_expat]


[builddirs_expat]
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f/

[libs_expat]
expat

[system_libs_expat]


[defines_expat]
XML_STATIC

[cppflags_expat]


[cxxflags_expat]


[cflags_expat]


[sharedlinkflags_expat]


[exelinkflags_expat]


[sysroot_expat]


[frameworks_expat]


[frameworkdirs_expat]


[rootpath_expat]
/root/.conan/data/expat/2.4.8/_/_/package/2bf99ee01cda593d5986afc64acf262ad576354f

[name_expat]
expat

[version_expat]
2.4.8

[generatornames_expat]
cmake_find_package=EXPAT
cmake_find_package_multi=expat

[generatorfilenames_expat]



[includedirs_sqlite3]
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/include

[libdirs_sqlite3]
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/lib

[bindirs_sqlite3]
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/bin

[resdirs_sqlite3]


[builddirs_sqlite3]


[libs_sqlite3]
sqlite3

[system_libs_sqlite3]
pthread
dl
m

[defines_sqlite3]


[cppflags_sqlite3]


[cxxflags_sqlite3]


[cflags_sqlite3]


[sharedlinkflags_sqlite3]


[exelinkflags_sqlite3]


[sysroot_sqlite3]


[frameworks_sqlite3]


[frameworkdirs_sqlite3]


[rootpath_sqlite3]
/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8

[name_sqlite3]
sqlite3

[version_sqlite3]
3.39.3

[generatornames_sqlite3]
cmake_find_package=SQLite
cmake_find_package_multi=SQLite

[generatorfilenames_sqlite3]
cmake_find_package=SQLite3
cmake_find_package_multi=SQLite3


[includedirs_libpq]
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

[libdirs_libpq]
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

[bindirs_libpq]
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/bin

[resdirs_libpq]


[builddirs_libpq]
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

[libs_libpq]
pq
pgcommon
pgcommon_shlib
pgport
pgport_shlib

[system_libs_libpq]
pthread

[defines_libpq]


[cppflags_libpq]


[cxxflags_libpq]


[cflags_libpq]


[sharedlinkflags_libpq]


[exelinkflags_libpq]


[sysroot_libpq]


[frameworks_libpq]


[frameworkdirs_libpq]


[rootpath_libpq]
/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

[name_libpq]
libpq

[version_libpq]
14.2

[generatornames_libpq]
cmake_find_package=PostgreSQL
cmake_find_package_multi=PostgreSQL

[generatorfilenames_libpq]



[includedirs_libmysqlclient]
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/include

[libdirs_libmysqlclient]
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/lib

[bindirs_libmysqlclient]
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/bin

[resdirs_libmysqlclient]


[builddirs_libmysqlclient]
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126/

[libs_libmysqlclient]
mysqlclient

[system_libs_libmysqlclient]
stdc++
m
resolv

[defines_libmysqlclient]


[cppflags_libmysqlclient]


[cxxflags_libmysqlclient]


[cflags_libmysqlclient]


[sharedlinkflags_libmysqlclient]


[exelinkflags_libmysqlclient]


[sysroot_libmysqlclient]


[frameworks_libmysqlclient]


[frameworkdirs_libmysqlclient]


[rootpath_libmysqlclient]
/root/.conan/data/libmysqlclient/8.0.29/_/_/package/b554ea855a03a5ade04f150d5b3fe6326195d126

[name_libmysqlclient]
libmysqlclient

[version_libmysqlclient]
8.0.29

[generatornames_libmysqlclient]
pkg_config=mysqlclient
cmake_find_package=MySQL
cmake_find_package_multi=MySQL

[generatorfilenames_libmysqlclient]



[includedirs_zlib]
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

[libdirs_zlib]
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

[bindirs_zlib]


[resdirs_zlib]


[builddirs_zlib]
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

[libs_zlib]
z

[system_libs_zlib]


[defines_zlib]


[cppflags_zlib]


[cxxflags_zlib]


[cflags_zlib]


[sharedlinkflags_zlib]


[exelinkflags_zlib]


[sysroot_zlib]


[frameworks_zlib]


[frameworkdirs_zlib]


[rootpath_zlib]
/root/.conan/data/zlib/1.2.12/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

[name_zlib]
zlib

[version_zlib]
1.2.12

[generatornames_zlib]
cmake_find_package=ZLIB
cmake_find_package_multi=ZLIB

[generatorfilenames_zlib]



[includedirs_bzip2]
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/include

[libdirs_bzip2]
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/lib

[bindirs_bzip2]
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/bin

[resdirs_bzip2]


[builddirs_bzip2]
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/

[libs_bzip2]
bz2

[system_libs_bzip2]


[defines_bzip2]


[cppflags_bzip2]


[cxxflags_bzip2]


[cflags_bzip2]


[sharedlinkflags_bzip2]


[exelinkflags_bzip2]


[sysroot_bzip2]


[frameworks_bzip2]


[frameworkdirs_bzip2]


[rootpath_bzip2]
/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e

[name_bzip2]
bzip2

[version_bzip2]
1.0.8

[generatornames_bzip2]
cmake_find_package=BZip2
cmake_find_package_multi=BZip2

[generatorfilenames_bzip2]



[includedirs_openssl]
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

[libdirs_openssl]
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

[bindirs_openssl]
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/bin

[resdirs_openssl]


[builddirs_openssl]


[libs_openssl]
ssl
crypto

[system_libs_openssl]
dl
pthread
rt

[defines_openssl]


[cppflags_openssl]


[cxxflags_openssl]


[cflags_openssl]


[sharedlinkflags_openssl]


[exelinkflags_openssl]


[sysroot_openssl]


[frameworks_openssl]


[frameworkdirs_openssl]


[rootpath_openssl]
/root/.conan/data/openssl/1.1.1q/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

[name_openssl]
openssl

[version_openssl]
1.1.1q

[generatornames_openssl]
cmake_find_package=OpenSSL
cmake_find_package_multi=OpenSSL

[generatorfilenames_openssl]



[includedirs_zstd]
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6/include

[libdirs_zstd]
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6/lib

[bindirs_zstd]


[resdirs_zstd]


[builddirs_zstd]


[libs_zstd]
zstd

[system_libs_zstd]
pthread

[defines_zstd]


[cppflags_zstd]


[cxxflags_zstd]


[cflags_zstd]


[sharedlinkflags_zstd]


[exelinkflags_zstd]


[sysroot_zstd]


[frameworks_zstd]


[frameworkdirs_zstd]


[rootpath_zstd]
/root/.conan/data/zstd/1.5.2/_/_/package/1e8f95ed1a9a9ef594f731500716088f6eb8bee6

[name_zstd]
zstd

[version_zstd]
1.5.2

[generatornames_zstd]


[generatorfilenames_zstd]



[includedirs_lz4]
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/include

[libdirs_lz4]
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/lib

[bindirs_lz4]


[resdirs_lz4]


[builddirs_lz4]
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646/

[libs_lz4]
lz4

[system_libs_lz4]


[defines_lz4]


[cppflags_lz4]


[cxxflags_lz4]


[cflags_lz4]


[sharedlinkflags_lz4]


[exelinkflags_lz4]


[sysroot_lz4]


[frameworks_lz4]


[frameworkdirs_lz4]


[rootpath_lz4]
/root/.conan/data/lz4/1.9.3/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646

[name_lz4]
lz4

[version_lz4]
1.9.3

[generatornames_lz4]
pkg_config=liblz4

[generatorfilenames_lz4]



[USER_bzip2]
[USER_expat]
[USER_libmysqlclient]
[USER_libpq]
[USER_lz4]
[USER_openssl]
[USER_pcre2]
[USER_poco]
[USER_sqlite3]
[USER_zlib]
[USER_zstd]
[ENV_poco]
[ENV_pcre2]
PATH=["/root/.conan/data/pcre2/10.40/_/_/package/1dfef2d0e403379e1d4f34660b435fba4caeec11/bin"]
[ENV_expat]
[ENV_sqlite3]
PATH=["/root/.conan/data/sqlite3/3.39.3/_/_/package/8fc930bdbf940331d9f322f454eac3dd06fd27e8/bin"]
[ENV_libpq]
PostgreSQL_ROOT=/root/.conan/data/libpq/14.2/_/_/package/dfbe50feef7f3c6223a476cd5aeadb687084a646
[ENV_libmysqlclient]
[ENV_zlib]
[ENV_bzip2]
PATH=["/root/.conan/data/bzip2/1.0.8/_/_/package/c32092bf4d4bb47cf962af898e02823f499b017e/bin"]
[ENV_openssl]
[ENV_zstd]
[ENV_lz4]